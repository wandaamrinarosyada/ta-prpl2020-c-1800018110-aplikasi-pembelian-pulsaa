<?php include "header.php"; ?>

<!DOCTYPE html>
<html lang="en">

<body>

<div class="container">
  <h2>Tambah Pembelian</h2>
  <form action="proses_inputpembelian.php" method="POST">
    <div class="form-group">
      <label for="email">ID Pembelian:</label>
      <input type="text" class="form-control" placeholder="Masukkan ID Pembelian" name="Id_Pembelian"required>
    </div>
    <div class="form-group">
      <label for="email">Nomor Telpon:</label>
      <input type="text" class="form-control" placeholder="Masukkan Nomor Telpon" name="No_Tlp"required>
    </div>
    <div class="form-group">
      <label for="email">Tanggal Transaksi</label>
      <input type="date" class="form-control" placeholder="Masukkan Tanggal Transaksi" name="Tanggal_Transaksi"required>
    </div>
    <div class="form-group">
      <label for="pwd">Id Admin</label>
      <input type="text" class="form-control" id="pwd" placeholder="Masukkan Id Admin" name="Id_Admin"required>
    </div>
    <div class="form-group">
      <label for="pwd">Id Pulsa</label>
      <input type="text" class="form-control" id="pwd" placeholder="Masukkan Id Pulsa" name="Id_Pulsa"required>
    </div>
    <div class="form-group">
      <label for="pwd">Jumlah Pembelian</label>
      <input type="text" class="form-control" id="pwd" placeholder="Masukkan Jumlah Pembelian" name="Jumlah_Pembelian"required>
    </div>
    <div class="form-group">
      <label for="pwd">Total Pembelian</label>
      <input type="text" class="form-control" id="pwd" placeholder="Masukkan Total Pembelian" name="Total_Pembelian"required>
    </div>
     
    <div class="form-group form-check">
      <label class="form-check-label">
        <input class="form-check-input" type="checkbox" name="remember"> Remember me
      </label>
    </div>
    <button type="submit" class="btn btn-primary">Submit</button>
  </form>
</div>

</body>
</html>
