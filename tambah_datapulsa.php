<?php include "header.php"; ?>

<!DOCTYPE html>
<html lang="en">

<body>

<div class="container">
  <h2>Tambah Pulsa</h2>
  <form action="proses_inputpulsa.php" method="POST">
    <div class="form-group">
      <label for="email">ID Pulsa:</label>
      <input type="text" class="form-control" placeholder="Masukkan Id Pulsa" name="Id_Pulsa"required>
    </div>
    <div class="form-group">
      <label for="email">Jenis Produk</label>
      <input type="text" class="form-control" placeholder="Masukkan Jenis Produk" name="Jenis_Produk"required>
    </div>
    <div class="form-group">
      <label for="pwd">Harga Beli</label>
      <input type="text" class="form-control" id="pwd" placeholder="Masukkan Harga Beli" name="Harga_Beli"required>
    </div>
    <div class="form-group">
      <label for="pwd">Harga Jual</label>
      <input type="text" class="form-control" id="pwd" placeholder="Masukkan Harga Jual" name="Harga_Jual"required>
    </div>
     
    <div class="form-group form-check">
      <label class="form-check-label">
        <input class="form-check-input" type="checkbox" name="remember"> Remember me
      </label>
    </div>
    <button type="submit" class="btn btn-primary">Submit</button>
  </form>
</div>

</body>
</html>
