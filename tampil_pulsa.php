<?php include 'header.php';
$data = mysqli_query($koneksi, "SELECT * FROM pulsa");
 ?>
 
 <div class="container">
		 	<div class="page-header">
		       <h3>Data Pulsa</h3>
		    </div>
		    <table class="table table-bordered table-striped">
		    	<form method="post" enctype="multipart/form-data">
            <div class="form-group row mt-2">
                    <div class="col-sm-10">
                      <input type="text" name="keyword" class="form-control" id="">
                    </div>
                    <button type="submit" class="btn btn-sm btn-success" name="cari">Cari</button>
                  </div>
            </form>
	 	<tr>
	 		<td>ID Pulsa</td>
	 		<td>Jenis Produk</td>
	 		<td>Harga Beli</td>
	 		<td>Harga Jual</td>
	 		 <td>Action</td>
	 	</tr>

	 	<?php foreach($data as $value): ?>
	 	<tr>
	 		<td><?php echo $value ['Id_Pulsa'] ?></td>
	 		<td><?php echo $value ['Jenis_Produk'] ?></td>
	 		<td><?php echo $value ['Harga_Beli'] ?></td>
	 		<td><?php echo $value ['Harga_Jual'] ?></td>
	 		<td> 
	 			<a href="hapus_pulsa.php?Id_Pulsa=<?php echo $value ['Id_Pulsa'] ?>"> Hapus </a> 
	 			<a href="edit_pulsa.php?Id_Pulsa=<?php echo $value ['Id_Pulsa'] ?>"> Edit </a>

	 		</td>

	 	</tr>
	 	<?php endforeach ?>
	 	<p></p>
	 	<a href="tambah_datapulsa.php">Tambah data </a>
	</table>
	<br>
		<p align="left"><a href="pencariann_datapulsa.php"><li>Mencari Data Pulsa</li></a></p>
	</div>
</div>

<?php include "footer.php"; ?>