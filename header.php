<?php
session_start();
if(!isset($_SESSION['Login'])){
	header('location:login.php');
}

include "koneksi.php";
?>

<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">
    <meta rel="icon" href="./assets/favicon.png">

    <title>Aplikasi Pembelian Pulsa</title>

    <link href="./assets/css/bootstrap.min.css" rel="stylesheet">
    <link href="./assets/style.css" rel="stylesheet">

</head>
<body>
<!---<h3>Aplikasi Pembelian Pulsa</h3>
<hr/>
<a href="tampil_admin.php">Data Admin</a> |
<a href="tampil_pembelian.php">Data Pembelian</a> |
<a href="tampil_pulsa.php">Data Pulsa</a> |
<a class="pencarian">
	<form name="Jenis_Produk" onSubmit="cari()">Search<input name="keyword" size="15" type="text"><input type="submit" value="Go"></form>	
</a> |
<a href="logout.php">Logout</a> |
<hr/> --->

<!-- Fixed navbar -->
	<nav class="navbar navbar-expand-md navbar-dark fixed-top bg-dark">
    <div class="navbar navbar-default navbar-fixed-top" role="navigation">
      <div class="container">
        <div class="navbar-header">
          <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
            <span class="sr-only">Toggle navigation</span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
          </button>
          <a class="navbar-brand" href="./">Aplikasi Pembelian Pulsa</a>
        </div>
        <div class="collapse navbar-collapse">
          <ul class="nav navbar-nav">
            <li><a href="tampil_admin.php">Data Admin</a></li>
            <li><a href="tampil_pembelian.php">Data Pembelian</a></li>
            <li><a href="tampil_pulsa.php">Data Pulsa</a></li>
            <li><a href="logout.php">Logout</a></li>
            <li><a class="pencarian">
	        <form name="Jenis_Produk" onSubmit="cari()">Search<input name="keyword" size="15" type="text"><input type="submit" value="Go"></form></a></li>
          </ul>
        </div><!--/.nav-collapse -->
        </div>
    </div>
    </nav>
